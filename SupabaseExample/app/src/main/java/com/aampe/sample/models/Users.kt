package com.aampe.sample.models

import kotlinx.serialization.Serializable

/** Data Model to be sent to Supabase server*/
@Serializable
data class users(
    val id: String = "",
    val pushToken: String = "",
    val email: String = "",
    val name: String = "",
    val platform: String = "",
)