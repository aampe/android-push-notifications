package com.aampe.sample.utils

/**Singleton class holds constants for the app */
object AppConstants {

    const val EXTRA_MESSAGE_ID = "EXTRA_MESSAGE_ID"
}