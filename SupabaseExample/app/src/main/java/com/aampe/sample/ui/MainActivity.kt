package com.aampe.sample.ui

import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.aampe.sample.AampeApplication
import com.aampe.sample.R
import com.aampe.sample.databinding.ActivityMainBinding
import com.aampe.sample.models.users
import com.aampe.sample.utils.AppConstants
import com.aampe.sample.utils.AppPreferences
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.createSupabaseClient
import io.github.jan.supabase.postgrest.Postgrest
import io.github.jan.supabase.postgrest.postgrest
import kotlinx.coroutines.launch

/**Home Screen for Logged in User. This screen is used to demonstrate a logged user flow, logout flow and saving data on server with FCM Token */
class MainActivity : AppCompatActivity() {

    private var fcmMessageId: String = ""
    private lateinit var binding: ActivityMainBinding
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private var supabase: SupabaseClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("MainActivity", "IF APP IS NOT OPENED")
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        //Initialize supabase client using credentials got from project settings on supabase.
        supabase = createSupabaseClient(
            supabaseUrl = getString(R.string.supabase_url),
            supabaseKey = getString(R.string.supabase_key)
        ) {
            install(Postgrest)
        }

        // Obtain the FirebaseAnalytics instance.
        firebaseAnalytics = Firebase.analytics
        fcmMessageId = intent.getStringExtra(AppConstants.EXTRA_MESSAGE_ID) ?: ""
        binding.tvUsername.text = "Welcome " + AppPreferences.username + " !"
        binding.btnLogout.setOnClickListener {
            logoutTheUser()
        }
        getFCMTokenAndSaveLoggedInUser()
    }

    /** This function Logs Out the user of the Application and removes his FCM token from Supabase table.
     * Also this removes his locally stored data and navigates him to the Sign In Screen.*/
    private fun logoutTheUser() {
        lifecycleScope.launch {
            val users = users(
                AppPreferences.authUserId,
                "",
                AppPreferences.emailId,
                AppPreferences.username,
                (application as AampeApplication).getDeviceIdValue()
            )
            // Inserts/Updates the data for logged in user in Supabase table
            supabase!!.postgrest["users"].upsert(users)
        }

        Firebase.auth.signOut()
        AppPreferences.clearPreferences()
        startActivity(Intent(this, SplashActivity::class.java))
        finish()

    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        fcmMessageId = intent?.getStringExtra(AppConstants.EXTRA_MESSAGE_ID) ?: ""
        logNotificationEvent(fcmMessageId)
    }

    private fun logNotificationEvent(messageId: String) {
        if (!messageId.isNullOrEmpty()) {
            Log.d("MainActivity", "Logging Message ID: " + messageId)
            firebaseAnalytics.logEvent("push_notification_click") {
                param("aampe_message_id", messageId)
            }
        }
    }

    /** This function fetches the FCM token and save it to supabase with logged in user's data.*/
    private fun getFCMTokenAndSaveLoggedInUser() {
        Firebase.messaging.token.addOnCompleteListener(
            OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                val token = task.result

                // Log and toast
                val msg = getString(R.string.msg_token_fmt, token)
                Log.d(TAG, msg)

                lifecycleScope.launch {
                    val postgrestResult = supabase!!.postgrest["users"].select {
                        this.filter {
                            this.eq("id", (application as AampeApplication).getDeviceIdValue())
                        }
                    }
                    if (postgrestResult != null) {
                        supabase!!.postgrest["users"].delete {
                            this.filter {
                                this.eq("id", (application as AampeApplication).getDeviceIdValue())
                            }
                        }
                    }

                    val users = users(
                        AppPreferences.authUserId,
                        token,
                        AppPreferences.emailId,
                        AppPreferences.username,
                        (application as AampeApplication).getDeviceIdValue()
                    )
                    // Inserts/Updates the data for logged in user in Supabase table
                    supabase!!.postgrest["users"].upsert(users)
                }
            }
        )
    }


}