package com.aampe.sample.ui

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.aampe.sample.AampeApplication
import com.aampe.sample.R
import com.aampe.sample.databinding.ActivityGuestUserBinding
import com.aampe.sample.models.users
import com.aampe.sample.utils.AppPreferences
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.createSupabaseClient
import io.github.jan.supabase.postgrest.Postgrest
import io.github.jan.supabase.postgrest.postgrest
import kotlinx.coroutines.launch

/** This screen is used to demonstrate a sample guest user flow and saving data on server with FCM Token*/
class GuestUserActivity : AppCompatActivity() {
    private lateinit var binding: ActivityGuestUserBinding
    private var supabase: SupabaseClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_guest_user)

        //Initialize supabase client using credentials got from project settings on supabase.
        supabase = createSupabaseClient(
            supabaseUrl = getString(R.string.supabase_url),
            supabaseKey = getString(R.string.supabase_key)
        ) {
            install(Postgrest)
        }
        binding.ivBack.setOnClickListener { onBackPressedDispatcher.onBackPressed() }
        getFCMTokenAndSaveGuestUser()
    }

    /** This function fetches the FCM token and save it to supabase with unique device id.*/
    private fun getFCMTokenAndSaveGuestUser() {
        Firebase.messaging.token.addOnCompleteListener(
            OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(
                        ContentValues.TAG,
                        "Fetching FCM registration token failed",
                        task.exception
                    )
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                val token = task.result

                //Implement this method to send token to the supabase server.
                val authId =
                    if (AppPreferences.authUserId.isNullOrEmpty()) (application as AampeApplication).getDeviceIdValue() else AppPreferences.authUserId
                val users = users(
                    authId,
                    token,
                    AppPreferences.emailId,
                    AppPreferences.username,
                    (application as AampeApplication).getDeviceIdValue()
                )
                lifecycleScope.launch {
                    // Inserts/Updates the data for guest user in Supabase table
                    supabase!!.postgrest["users"].upsert(users)
                }
            }
        )
    }
}