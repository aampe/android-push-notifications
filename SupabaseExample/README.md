# Guide for Supabase Integration in Android

## Introduction to Supabase Android SDK
The Supabase Android SDK is a client-side library that enables Android Apps to interact with Supabase services seamlessly. It provides a range of functionalities from authentication to real-time database updates, making it a powerful tool for building robust applications with Supabase as the backend.


## STEP 1: Create new project on Supabase
1. Login to your Supabase account and go to [project](https://supabase.com/dashboard/projects)
2. Click on `New project` button.
3. Fill the required informations like Organisation, name, etc and click on `Create new project` like below image
   ![Create project](../Images/12.png)


## STEP 2: Get API keys and secrets
- After project is created successfully, click on the gear icon (project settings) on the bottom left corner of the page and select API from the menu.
  ![copy secrets](../Images/13.png)
- From the above page copy the `URL` (fond under Project URL heading) and `anon` key from Project API keys section. We will need these values while initialising Supabase swift client.


## STEP 3: Create database tables
- Select `Table editor` from the left menu and click on the `create new table` button.
  ![Create table](../Images/14.png)
- Fill the table name like name and description
- Create all the columns like
- id
- name
- email
- pushToken
- platform
- Click on `Save` button.
  ![create table](../Images/15.png)



## STEP 4: Set security rules
Once table is created we need to set security rules.
- Click on `No active RLS police` as shown below-
  ![set policy](../Images/16.png)
- Click on `New policy` and clcik on `Get started quickly`
  ![set policy](../Images/17.png)
- Select the policy template according to your project needs

## STEP 5: Add Supabase Android client


1. In your **module (app-level)** Gradle file (usually `<project>/<app-module>/build.gradle.kts` or `<project>/<app-module>/build.gradle`), add the Supabase dependencies.

```groovy  
//Supabase dependencies  
implementation(platform("io.github.jan-tennert.supabase:bom:2.1.3"))  
implementation("io.github.jan-tennert.supabase:postgrest-kt")  
implementation("io.ktor:ktor-client-android:2.3.8")  
```
**NOTE :  Adding Ktor Client Engine to each of your Kotlin targets is required**

2. Add **SERIALIZATION** if not added in you project.
- supabase-kt provides several different ways to encode and decode your custom objects. By default, [KotlinX Serialization](https://github.com/Kotlin/kotlinx.serialization) is used.

```groovy 
plugins {  
kotlin("plugin.serialization") version "KOTLIN_VERSION"  
}
```
```kotlin
val supabase = createSupabaseClient(supabaseUrl, supabaseKey) {  
//Already the default serializer, but you can provide a custom Json instance (optional):  
defaultSerializer = KotlinXSerializer(Json {  
//apply your custom config  
	})  
}
```

## STEP 6: Working with Supabase (Android Implementation)

### 1. Initialising Supabase client in the app.
### Create Supabase Client

Independently of which Supabase module you are using, you will need to initialize the main client first and install the module.
To create a new client, you can use the `createSupabaseClient` function.
When installing a module, you can pass a block to configure it.
```kotlin
private var supabase: SupabaseClient? = null  
  
override fun onCreate(savedInstanceState: Bundle?) {  
    super.onCreate(savedInstanceState)   
    //Initialize supabase client using credentials got from project settings on supabase.  
  supabase = createSupabaseClient(  
	supabaseUrl = "https://xyzcompany.supabase.co",
    supabaseKey = "public-anon-key"
  ) {  
	  install(Postgrest)  
    }
```

### 2. Create a serializable data class to store data
```kotlin
@Serializable  
data class users(  
	 val id: String = "",  
	 val pushToken: String = "",  
	 val email: String = "",  
	 val name: String = "",  
	 val deviceid: String = "",  
)
```

### 3. Save data to supabase table

```kotlin
val users = users(  
	  "Auth/UserId of the logged in User",  
	  "Your FCM Token",  
	  "email@example.com",  //User's email Id
	  "username",  
	  "Unique device id to detect device" 
)  
lifecycleScope.launch {  
  // Inserts/Updates the data for guest user in Supabase table  
  supabase!!.postgrest["users"].upsert(users)  
}
```
**Upsert** function inserts or updates the data to supabase table. If you only
want to save data you can call **insert** method only.

All the available operations/functions can be found [here.](https://supabase.com/docs/reference/kotlin/select)

### 4. Read data from Supabase Table

Perform a SELECT query on the table or view.

-   When calling a  `decode`  method, you have to provide a  [serializable class](https://supabase.com/docs/reference/kotlin/installing#serialization)  as the type parameter.
-   You can provide a  `Columns`  object to select specific columns.
-   You can provide a  [filter](https://supabase.com/docs/reference/kotlin/using-filters)  block to filter the results

```kotlin
val postgrestResult = supabase!!.postgrest["users"].select {  
  this.filter {  
  // you can apply any filtere here. 
    }  
}
```