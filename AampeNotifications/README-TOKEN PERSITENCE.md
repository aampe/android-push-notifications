# Guide to Persist FCM Token across Database.

### NOTE : We will use firestore here to save FCM token. Developers can use any type of Database/APIs to save this data.


## STEP 1 : Setting up Firestore in android app.
Before you can add Firestore to your Android app, you need to create a Firebase project to connect to your Android app. Step by step process is provided in [README.md](https://gitlab.com/aampe/android-push-notifications/-/blob/main/README.md?ref_type=heads) file.

1.  Follow the instructions to  [add Firebase to your Android app](https://gitlab.com/aampe/android-push-notifications/-/blob/main/README.md?ref_type=heads).

2. Using the [Firebase Android BoM](https://firebase.google.com/docs/android/learn-more#bom), declare the dependency for the Cloud Firestore library for Android in your **module (app-level) Gradle file** (usually `app/build.gradle.kts` or `app/build.gradle`).

```groovy
dependencies 
{  
dependencies {  // Import the BoM for the Firebase platform 
implementation(platform("com.google.firebase:firebase-bom:32.7.2")) 

// Declare the dependency for the Cloud Firestore library**  
// When using the BoM, you don't specify versions in Firebase library 
dependencies  implementation("com.google.firebase:firebase-firestore") 
}
```

## STEP 2 : Implementation to save data to FireStore


### 1.  Initialize an instance of Cloud Firestore

- Access a Cloud Firestore instance from your Activity

```kotlin
	val db =  Firebase.firestore
```
### 2 . Add Data to Firestore

- Cloud Firestore stores data in Documents, which are stored in Collections. Cloud Firestore creates collections and documents implicitly the first time you add data to the document. You do not need to explicitly create collections or documents.

Create a new collection and a document using the following example code.
```kotlin
// Create a new user with a first, middle, and last name  
val user = hashMapOf
("id" to "userId/Auth Id of the logged in User",
"pushToken" to "Your FMC token will be added here",
"email" to "example@gmail.com",
"deviceId" to "Unique Id to recognize device",  
)  
  
// Add a new document with a generated ID  
db.collection("users")  
.add(user)  
.addOnSuccessListener { documentReference ->  
Log.d(TAG,"DocumentSnapshot added with ID: ${documentReference.id}")  
}  
.addOnFailureListener { e ->
  Log.w(TAG,  "Error adding document", e) 
}
```

### 3.  Read data from Firestore
- Use the data viewer in the  [Firebase console](https://console.firebase.google.com/project/_/firestore/data)  to quickly verify that you've added data to Cloud Firestore.

You can also use the "get" method to retrieve the entire collection.

```kotlin
db.collection("users")  
.get()  
.addOnSuccessListener { result ->  
	for  (document in result)  {  
		Log.d(TAG,  "${document.id} => ${document.data}")  
	}  
}  
.addOnFailureListener { exception ->  
	Log.w(TAG,  "Error getting documents.", exception)  
}
```

## That's it! Your application is ready to store and Read data with Firestore.

## BONUS : Now we will learn a basic strategy to store and persist FCM token for a guest and logged in user on the same device.

Following are the scenarios and their effect on Firestore DB

### GUEST USER FLOW :

**APP :**
1.  Guest user installs the application and Go to SignIn Screen
2.  User clicks “I am Guest User”
3.  User navigates to a screen where he can access guest user functionalities (ONLY FOR DEMO)

**DB :**
1. A new entry will be created with (deviceId) as documentID having the data For Example:  
   deviceId: “7448b3038a1fdfbc"  
   pushToken:"fecQIwm8TaawLbut9sxBKJ:APA91bGnwp89ScIhS041_Aw5nwDizKdjLZ5pEsuAIPntegJ_2rOBLbZzNVsDqvl5lpMpD7oSp7SS-DyLhwHSQ454_WnSrAg0M66L8GLuEXF7RFElucPSRbTYURf68sCb3QzC21vFlB-k"

### GUEST USER FLOW AFTER LOGIN :

**APP :**
1.  Guest user installs the application and Go to SignIn Screen
2.  User clicks “I am Guest User”
3.  User navigates to a screen where he can access guest user functionalities (ONLY FOR DEMO)
4.  User goes back to SignIn screen and Logs In and Gets navigated to Home Screen

**DB :**
1. Document Id with DeviceId generated earlier will be deleted as no more needed
2.  User will be created/updated with his AUTHID as document with the updated deviceId,pushToken,email,id,name

### NORMAL USER FLOW AFTER LOGIN :

**APP :**
1.  Guest user installs the application and Go to SignIn Screen
2.  User Logs In and Gets navigated to Home Screen

**DB :**
1.  User will be created/updated with his AUTHID as document with the updated deviceId,pushToken,email,id,name

### USER LOGS OUT :

**APP :**
1.  User clicks on Logout button on Home Screen

**DB :**
1.  Push Token will be removed for the User with the given AUTHID


