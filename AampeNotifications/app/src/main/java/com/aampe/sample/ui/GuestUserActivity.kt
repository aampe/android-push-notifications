package com.aampe.sample.ui

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.aampe.sample.AampeApplication
import com.aampe.sample.R
import com.aampe.sample.databinding.ActivityGuestUserBinding
import com.aampe.sample.models.users
import com.aampe.sample.utils.AppPreferences
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.firestore.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging

/** This screen is used to demonstrate a sample guest user flow and saving data on Firestore with FCM Token*/
class GuestUserActivity : AppCompatActivity() {
    private lateinit var binding: ActivityGuestUserBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_guest_user)
        binding.ivBack.setOnClickListener { onBackPressedDispatcher.onBackPressed() }
        getFCMTokenAndSaveGuestUser()
    }

    /** This function fetches the FCM token and save it to Firestore with unique device id.*/
    private fun getFCMTokenAndSaveGuestUser() {
        Firebase.messaging.token.addOnCompleteListener(
            OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(
                        ContentValues.TAG,
                        "Fetching FCM registration token failed",
                        task.exception
                    )
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                val token = task.result

                //Implement this method to send token to Firestore.
                val db = com.google.firebase.Firebase.firestore
                val dbUsers = db.collection("users")
                val users = users(
                    AppPreferences.authUserId,
                    token,
                    AppPreferences.emailId,
                    AppPreferences.username,
                    (application as AampeApplication).getDeviceId()
                )
                dbUsers.document((application as AampeApplication).getDeviceId()).set(users)
                    .addOnSuccessListener {
                        Log.d("GUESTUSERACTIVITY", "DocumentSnapshot added successfully")
                    }.addOnFailureListener {
                    Log.w("GUESTUSERACTIVITY", "Error adding document", it)
                }
            }
        )
    }
}