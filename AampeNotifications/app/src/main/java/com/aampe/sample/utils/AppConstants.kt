package com.aampe.sample.utils

object AppConstants {

    const val EXTRA_MESSAGE_ID = "EXTRA_MESSAGE_ID"
    const val EXTRA_CUSTOM_PAYLOAD = "EXTRA_CUSTOM_PAYLOAD"
}