package com.aampe.sample

import android.app.Application
import android.provider.Settings
import com.aampe.sample.utils.AppPreferences

class AampeApplication : Application() {
    private lateinit var uniqueID: String

    override fun onCreate() {
        super.onCreate()

        //Fetching Unique Id from device to detect device
        uniqueID = Settings.Secure.getString(
            applicationContext.contentResolver,
            Settings.Secure.ANDROID_ID
        )

        //Method to initialize preferences for storing data locally
        AppPreferences.init(this)
    }

    fun getDeviceId(): String {
        return uniqueID
    }

}