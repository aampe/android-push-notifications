package com.aampe.sample.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.aampe.sample.R
import com.aampe.sample.databinding.ActivitySplashBinding
import com.aampe.sample.utils.AppConstants
import com.aampe.sample.utils.AppPreferences

/** Entry point of the app. Navigates user based on its state. */
class SplashActivity : AppCompatActivity() {
    lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)

        // FCM data for redirecting user to screens inside Home Screen
        val fcmMessageId = intent.getStringExtra(AppConstants.EXTRA_MESSAGE_ID) ?: ""

        //Animation for app logo on splash screen
        binding.ivSplash.visibility = View.VISIBLE
        val animation = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        //starting the animation
        binding.ivSplash.startAnimation(animation)

        //Checks if user is logged in or not based on stored username.
        //Logged out user will have null username
        Handler(Looper.getMainLooper()).postDelayed({
            if (AppPreferences.username.isNullOrEmpty())
                startActivity(Intent(this, SignInActivity::class.java))
            else
                startActivity(Intent(this, MainActivity::class.java).apply {
                    putExtra(AppConstants.EXTRA_MESSAGE_ID, fcmMessageId)
                })
            finish()
        }, 1200)
    }

}