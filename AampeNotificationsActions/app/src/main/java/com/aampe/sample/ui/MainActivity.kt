package com.aampe.sample.ui

import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.aampe.sample.AampeApplication
import com.aampe.sample.R
import com.aampe.sample.databinding.ActivityMainBinding
import com.aampe.sample.fcm.MyFirebaseMessagingService
import com.aampe.sample.models.NotificationActionData
import com.aampe.sample.models.users
import com.aampe.sample.utils.AppConstants
import com.aampe.sample.utils.AppPreferences
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import java.util.Calendar

/**Home Screen for Logged in User. This screen is used to demonstrate a logged user flow, logout flow and saving data on Firestore with FCM Token */
class MainActivity : AppCompatActivity() {

    private var fcmMessageId: String = ""
    private lateinit var binding: ActivityMainBinding
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        // Obtain the FirebaseAnalytics instance.
        firebaseAnalytics = Firebase.analytics
        fcmMessageId = intent.getStringExtra(AppConstants.EXTRA_MESSAGE_ID) ?: ""
        binding.tvUsername.text = "Welcome " + AppPreferences.username + " !"
        binding.btnLogout.setOnClickListener {
            logoutTheUser()
        }
        binding.btnSendNotification.setOnClickListener {
            MyFirebaseMessagingService().sendNotification("Aampe Push","This is a test message for user",Calendar.getInstance().time.toString(),null)
        }
        getFCMTokenAndSaveLoggedInUser()
    }

    /** This function Logs Out the user of the Application and removes his FCM token from Firestore table.
     * Also this removes his locally stored data and navigates him to the Sign In Screen.*/
    private fun logoutTheUser() {
        val db = com.google.firebase.Firebase.firestore
        val dbUsers = db.collection("users")
        val users = users(
            AppPreferences.authUserId,
            "",
            AppPreferences.emailId,
            AppPreferences.username,
            (application as AampeApplication).getDeviceId()
        )
        dbUsers.document(AppPreferences.authUserId).set(users).addOnSuccessListener {
            Log.d("MainActivity", "DocumentSnapshot added successfully")
            Firebase.auth.signOut()
            AppPreferences.clearPreferences()
            startActivity(Intent(this, SplashActivity::class.java))
            finish()
        }.addOnFailureListener {
            Log.w("MainActivity", "Error adding document", it)
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        fcmMessageId = intent?.getStringExtra(AppConstants.EXTRA_MESSAGE_ID) ?: ""
        Log.d("MainActivity", "IF APP IS ALREADY OPENED")
        logNotificationEvent(fcmMessageId)
    }

    private fun logNotificationEvent(messageId: String) {
        if (!messageId.isNullOrEmpty()) {
            Log.d("MainActivity", "Logging Message ID: " + messageId)
            firebaseAnalytics.logEvent("push_notification_click") {
                param("aampe_message_id", messageId)
            }
        }

    }

    /** This function fetches the FCM token and save it to Firestore with logged in user's data.*/
    private fun getFCMTokenAndSaveLoggedInUser() {
        Firebase.messaging.token.addOnCompleteListener(
            OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                val token = task.result

                //Implement this method to send token to Firestore.
                val db = com.google.firebase.Firebase.firestore
                val dbUsers = db.collection("users")

                //Check if device Id already there in Documents
                val docIdRef: DocumentReference =
                    dbUsers.document((application as AampeApplication).getDeviceId())
                docIdRef.get().addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val document = task.result
                        if (document.exists()) {
                            Log.d(TAG, "Document exists!")
                            docIdRef.delete().addOnCompleteListener { deleteTask ->
                                if (deleteTask.isSuccessful) {
                                    Log.d(TAG, "Document deleted successfully")
                                } else {
                                    Log.d(TAG, "Failed with: ", task.exception)
                                }
                            }
                        } else {
                            Log.d(TAG, "Document does not exist!")
                        }
                    } else {
                        Log.d(TAG, "Failed with: ", task.exception)
                    }
                }

                val users = users(
                    AppPreferences.authUserId,
                    token,
                    AppPreferences.emailId,
                    AppPreferences.username,
                    (application as AampeApplication).getDeviceId()
                )
                dbUsers.document(AppPreferences.authUserId).set(users).addOnSuccessListener {
                    Log.d("MainActivity", "DocumentSnapshot added successfully")
                }.addOnFailureListener {
                    Log.w("MainActivity", "Error adding document", it)
                }

//                docIdRefNotification.get().addOnCompleteListener { task ->
//                    if (task.isSuccessful) {
//                        val document = task.result
//                        docIdRefNotification.set(NotificationActionData("YEAH","Are you done with your work?"))
//                            .addOnCompleteListener {
//                                Log.d("TAG","NOTIFICATION DATA ADDED SUCCESSFULLY");
//                            }
//                            .addOnFailureListener {
//                            it.printStackTrace()
//                        }
//                    }
//                }
            }
        )
    }
}