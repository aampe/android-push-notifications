package com.aampe.sample.fcm

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.widget.Toast
import com.google.firebase.analytics.FirebaseAnalytics


class ActionsReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val message = intent?.getStringExtra("MESSAGE")
        if (message != null) {
            Toast.makeText(
                context,
                message,
                Toast.LENGTH_LONG
            ).show()
        }

        val manager = context?.getSystemService(NOTIFICATION_SERVICE) as NotificationManager?
        manager!!.cancel(0)
    }
}