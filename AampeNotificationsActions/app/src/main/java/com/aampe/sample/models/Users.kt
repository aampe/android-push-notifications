package com.aampe.sample.models

/** Data Model to be sent to Firestore*/
data class users(
    val id: String = "",
    val pushToken: String = "",
    val email: String = "",
    val name: String = "",
    val deviceId: String = ""
)