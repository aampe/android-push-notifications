package com.aampe.sample.models

/** Data Model to be sent to Firestore*/
data class NotificationActionData(
    val action: String = "",
    val message: String = "",
    val notificationType: Int = 1
)