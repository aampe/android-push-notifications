package com.aampe.sample.fcm


import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.provider.Settings
import android.util.Log
import androidx.core.app.NotificationCompat
import com.aampe.sample.R
import com.aampe.sample.ui.MainActivity
import com.aampe.sample.utils.AppConstants
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONArray

/** MyFirebaseMessagingService is responsible to get all the push notifications from FCM, parse them and navigation to the app from notifications */
class MyFirebaseMessagingService : FirebaseMessagingService() {

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        //Handle FCM messages here.
        Log.d(TAG, "From: ${remoteMessage.from}")
        Log.d(TAG, "Message ID: ${remoteMessage.messageId}")
        Log.d(TAG, "Data Payload: ${remoteMessage.data}")
        Log.d(TAG, "Notification Title: ${remoteMessage.notification?.title}")
        Log.d(TAG, "Notification Body: ${remoteMessage.notification?.body}")
        Log.d(TAG, "Message Type: ${remoteMessage.messageType}")
        Log.d(TAG, "Priority: ${remoteMessage.priority}")
        Log.d(TAG, "Sender Id: ${remoteMessage.senderId}")
        Log.d(TAG, "Sent Time: ${remoteMessage.sentTime}")


        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "Message data payload: ${remoteMessage.data}")
            val dataPayload = remoteMessage.data
            sendNotification(
                dataPayload["title"],
                dataPayload["body"].toString(),
                remoteMessage.messageId ?: "",
                dataPayload
            )
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    // [START on_new_token]
    /**
     * Called if the FCM registration token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the
     * FCM registration token is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // FCM registration token to your app server.
        sendRegistrationToServer(token)
    }
    // [END on_new_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String?) {
        //Implement this method to send token to your app server.
        Log.d(TAG, "sendRegistrationTokenToServer($token)")
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    fun sendNotification(
        messageTitle: String?,
        messageBody: String,
        messageId: String,
        data: MutableMap<String, String>?
    ) {
        val requestCode = 0
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra(AppConstants.EXTRA_MESSAGE_ID, messageId)
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this,
            requestCode,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT

        )


        val flag =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                PendingIntent.FLAG_IMMUTABLE
            else
                0

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_stat_name)
            .setContentTitle(messageTitle ?: "")
            .setContentText(messageBody)
            .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
            .setAutoCancel(true)
            .setPriority(NotificationManager.IMPORTANCE_MAX)
            .setColor(Color.parseColor("#A95B43"))
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

        //Parse the action buttons if given in the payload and add them to notifications.
        if (data != null) {
            val actionsStr = data.get("actions")
            if (!actionsStr.isNullOrEmpty()) {
                val jsonArray = JSONArray(actionsStr)
                Log.d(TAG, "ACTIONS SIZE: ${jsonArray.length()}")
                for (i in 0 until jsonArray.length()) {

                    if (i == 0) {
                        val actionTitle = jsonArray.getString(i)
                        val intentBroadcastAction1 = Intent(this, ActionsReceiver::class.java).apply {
                            putExtra("MESSAGE", actionTitle)
                        }
                        val pendingIntentAction1 = PendingIntent.getBroadcast(
                            this,
                            1111,
                            intentBroadcastAction1,
                            flag
                        )
                        notificationBuilder.addAction(0, actionTitle, pendingIntentAction1)
                    }
                    if (i == 1) {
                        val actionTitle = jsonArray.getString(i)
                        val intentBroadcastAction2 = Intent(this, ActionsReceiver::class.java).apply {
                            putExtra("MESSAGE", actionTitle)
                        }
                        val pendingIntentAction2 = PendingIntent.getBroadcast(
                            this,
                            2222,
                            intentBroadcastAction2,
                            flag
                        )
                        notificationBuilder.addAction(0, actionTitle, pendingIntentAction2)
                    }
                    if (i == 2) {
                        val actionTitle = jsonArray.getString(i)
                        val intentBroadcastAction3 = Intent(this, ActionsReceiver::class.java).apply {
                            putExtra("MESSAGE", actionTitle)
                        }
                        val pendingIntentAction3 = PendingIntent.getBroadcast(
                            this,
                            3333,
                            intentBroadcastAction3,
                            flag
                        )
                        notificationBuilder.addAction(0, actionTitle, pendingIntentAction3)
                    }

                }
            }
        }

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT,
            )
            notificationManager.createNotificationChannel(channel)
        }

        val notificationId = 0
        notificationManager.notify(notificationId, notificationBuilder.build())
    }

    companion object {
        private const val TAG = "MyFirebaseMsgService"
    }
}
