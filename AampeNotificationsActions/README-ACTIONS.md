
# Guide to Add Actions to the FCM Notifications.

### NOTE : We will use firestore here to save the data. Developers can use any type of Database/APIs to save this data.


## STEP 1 : What are Action Buttons in Notification?

A notification can offer up to **three action buttons** that let the user respond quickly, such as to snooze a reminder or to reply to a text message. But these action buttons must not duplicate the action performed when the user taps the notification.

1. To add an action button, pass a `PendingIntent` to the [`addAction()`](https://developer.android.com/reference/androidx/core/app/NotificationCompat.Builder#addAction(androidx.core.app.NotificationCompat.Action)) method. This is like setting up the notification's default tap action, except instead of launching an activity, you can do other things such as start a [`BroadcastReceiver`](https://developer.android.com/reference/android/content/BroadcastReceiver) that performs a job in the background so that the action doesn't interrupt the app that's already open.

```kotlin  
val ACTION_POSITIVE =  "PositiveAction"  
  
val actionIntent =  Intent(this,  ActionsReceiver::class.java).apply { action = ACTION_POSITIVE  
    putExtra(EXTRA_NOTIFICATION_ID,  0)  
}
val actionPendingIntent:PendingIntent = PendingIntent.getBroadcast(this,  0, actionIntent,  0)
val builder =  NotificationCompat.Builder(this, CHANNEL_ID)  
.setSmallIcon(R.drawable.notification_icon)  
.setContentTitle("Aampe Notification")  
.setContentText("Are you interested in more notifications?")
.setPriority(NotificationCompat.PRIORITY_DEFAULT)
.setContentIntent(pendingIntent)
.addAction(0, getString(R.string.action_yes), actionPendingIntent)
```  

## STEP 2 : Implementations of actions to the app.


### 1.  Create a Broadcast receiver

- You can create your own BroadcastReceiver. Here is a sample that we are using in the sample app:

```kotlin  
class ActionsReceiver : BroadcastReceiver() {  
    override fun onReceive(context: Context?, intent: Intent?) {  
        val message = intent?.getStringExtra("MESSAGE")  
        if (message != null) {  
            Toast.makeText(  
                context,  
  message,  
  Toast.LENGTH_LONG  
  ).show()  
        }  
  
        val manager = context?.getSystemService(NOTIFICATION_SERVICE) as NotificationManager?  
        manager!!.cancel(0)  
    }  
}
```  
### 2 .  Adding an action button:

- Let’s add an action button in our notification in the sample App. It is similar to setting the tap action by creating a pending intent only change is that we set it using **addAction()** to attach it to the notification. We can create intents for both activities or broadcast receivers as shown below:

```kotlin  
// adding action for activity

val activityActionIntent = Intent(application,
MainActivity::class.java).apply {
	flags = Intent.FLAG_ACTIVITY_NEW_TASK or 
	Intent.FLAG_ACTIVITY_CLEAR_TASK
}
val activityActionPendingIntent: PendingIntent =
PendingIntent.getActivity(application, 0, activityActionIntent, 0)

// adding action for broadcast
val broadcastIntent = Intent(application,
ActionsReceiver::class.java).apply {
putExtra("MESSAGE", "some message for toast")
}
val broadcastPendingIntent: PendingIntent =
PendingIntent.getBroadcast(application, 0, broadcastIntent, 0) 
```  

- Then add these actions to the builder like this:
```kotlin  
val builder = NotificationCompat.Builder(application, CHANNEL_CODE)
 .setSmallIcon(R.mipmap.ic_launcher_round) 
 .setContentTitle("Aampe Notification") 
 .setContentText("Are you interested in more notifications?") 
 .setPriority(NotificationCompat.PRIORITY_DEFAULT) 
 // Set the intent that will fire when the user taps the notification 
 .setContentIntent(tapPendingIntent) .setAutoCancel(true) 
 //for adding action 
 .addAction(0, "Go To App", activityActionPendingIntent)
 .addAction(0, "Yes", broadcastPendingIntent)
```  

And that’s all, this will give two action buttons and your defined actions.

![actions](../Images/18.png)

## STEP 2 : Customising Notification action buttons from FCM.

You can customise the notification buttons and titles from FCM message itself.
Look at the below payload :

```json
{
"to":
"fDri6znQQUS1j8UBJL1xQk:APA91bE_SRg9es39lB3tqyW8Tl0kv6tbxfXPrJ6yTxhut929iSPSbSmjUfnh3DL1mUeeD4fBUrApzjZRL-BC3qKPWy0IT1FFxIYdojXZ9htHGTY7T2Ji2T53VUza9wvJSysAUj6dwB4",
"data": {
"title": "Aampe Notification",
"body": "Are you interested in more notifications?",
"actions":[
"Go to App",
"Yes"
]
}
}
```
Here we have actions defined in the data payload. You can get the actions from the data payload and parse them and add them accordingly to your app.

